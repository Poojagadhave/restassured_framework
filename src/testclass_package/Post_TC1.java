package testclass_package;

import java.io.File;







import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.Test;

import commam_method_package.Trigger_API_Method;
import comman_utility_package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;
import request_repository.Post_Request_Repository;

public class Post_TC1 extends Post_Request_Repository {
	 @Test
	public static void executor() throws IOException {
		
		 String requestbody = post_TC1_Request();

		
		File dirname= Handle_API_Logs.Create_log_Directory("Post_TC1");
		for (int i = 0; i < 5; i++) {

			int Status_Code = Trigger_API_Method.extract_Status_Code(requestbody, post_endpoint());
			//System.out.println(Status_Code);

			if (Status_Code == 201) {
				
				String responseBody = Trigger_API_Method.extract_Response_Body(requestbody, post_endpoint());
				System.out.println("response Body :" + responseBody);
				//Handle_API_Logs.evidence_creator(dirname, "Post_TC1 ", post_endpoint(), post_TC1_Request(), responseBody);
				validator(requestbody,responseBody);
				break;
			} else {
				System.out.println("Desired status code not found hence,retry");
			}
		}

	}

	public static void validator(String requestbody,String responseBody)  throws IOException {

		JsonPath jsp = new JsonPath(responseBody);
		String res_name = jsp.getString("name");
		//System.out.println("Requestbody name" + req_name);
		String res_job = jsp.getString("job");
		//System.out.println("Requestbody job" + req_job);
		
	//String res_createdAt = Jsp_res.getString("createdAt").substring(0, 10);//
	//LocalDateTime CurrentDate = LocalDateTime.now();//
//	String ExceptedDate = CurrentDate.toString().substring(0, 10);//
	//System.out.println(ExpectedDate);//
		
		// validation

		Assert.assertEquals(res_name, "morpheus");
		Assert.assertEquals(res_job, "leader");
	}

}
