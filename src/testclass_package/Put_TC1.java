package testclass_package;

import java.io.File;








import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.Test;

import commam_method_package.Trigger_API_Method;
import comman_utility_package.Handle_API_Logs;
import io.restassured.path.json.JsonPath;
import request_repository.Put_Request_Repository;

public class Put_TC1 extends Put_Request_Repository {
    @Test
	public static void executor() throws IOException {
		
	String requestbody = put_TC1_Request();	
		
		File dirname = Handle_API_Logs.Create_log_Directory("Put_TC1");
		for (int i = 0; i < 5; i++) {

			int Status_Code = Trigger_API_Method.extract_Status_Code(requestbody, put_endpoint());
			System.out.println(Status_Code);

			if (Status_Code == 201) {
				String responseBody = Trigger_API_Method.extract_Response_Body(requestbody, put_endpoint());
				System.out.println("response Body :" + responseBody);
				//Handle_API_Logs.evidence_creator(dirname, "Put_TC1", put_endpoint(), requestbody, responseBody);
				validator(requestbody , responseBody);
				break;
			} else {
				System.out.println("Desired status code not found hence,retry");
			}
		}

	}
    
	public static void validator(String requestbody,String responseBody) {

		JsonPath jsp = new JsonPath(responseBody);
		String res_name = jsp.getString("name");
		String res_job = jsp.getString("job");

		// validation

		Assert.assertEquals(res_name, "morpheus");
		//Assert.assertEquals(res_job, "leader");
	}

}




