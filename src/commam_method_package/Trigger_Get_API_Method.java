package commam_method_package;

import static io.restassured.RestAssured.given;




import request_repository.Endpoints;

public class Trigger_Get_API_Method extends Endpoints{
	public static int extract_Status_Code(String URL) {
		   int Status_Code = given().header("Content-Type","application/json")
				   .when().get(URL).then().extract().statusCode();
		   return Status_Code;
				   
	}
public static String extract_Responsebody(String URL) {
	String Responsebody = given().header("Content-Type","application/json").log().all()
			.when().get(URL).then().log().all().extract().response().asString();
	        return Responsebody;
	
			
}
}


