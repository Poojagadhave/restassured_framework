package RestassuredreferenceCopy;

import static io.restassured.RestAssured.given;

import io.restassured.RestAssured;

public class Deletereference {

	public static void main(String[] args) {
		
	//step 1 : Declare the variables for base uri and request body
	String BaseURI="https://reqres.in/";
					
	//Step 2 : BaseURI
	RestAssured.baseURI=BaseURI;
			
	//step 2 : configure Request Body
					
	int statusCode=given()
	.header("Content-Type","application/json").when().delete("api/users/2")
	.then().extract().statusCode();
	System.out.println(statusCode);

	}

}
