package RestassuredreferenceCopy;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Patchreference {

	public static void main(String[] args) {
		
	//step 1 : Declare the  baseURI and request body
		
	String BaseURI="https://reqres.in/";

	String requestBody="{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"zion resident\"\r\n"
						+ "}";

	String Resource = "api/users/2";
				
				
	//Step 2 :  Declare BaseURI
				
	RestAssured.baseURI=BaseURI;
				
	// Step 3 : Configure your request body and triger the api

	String responsebody=
	given().header("Content-Type","application/json").body(requestBody)
	.when().post("Resource")
	.then().extract().response().asString();
		   
				
	/*given().header("Content-Type","application/json")
	 * .body(requestBody).log().all()
	 * .when()
	.patch(Resource)
	.then().log().all().extract().response().asString();*/
				
	//step 4 : create an object of JsonPath to parse the requestbody
		
	JsonPath jsp_req= new JsonPath(requestBody);
	String req_name=jsp_req.getString("name");
	System.out.println("requestbody parameter name:"+req_name);
	String req_job=jsp_req.getString("job");
	System.out.println("requestbody parameter job:"+req_job);
				
	//step 5 : Validate the responsebody parameters
	Assert.assertEquals(req_name, "morpheus");
	Assert.assertEquals(req_job, "zion resident");
				

	}

}
