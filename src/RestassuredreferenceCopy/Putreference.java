package RestassuredreferenceCopy;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Putreference {

	public static void main(String[] args) {
		
//step 1 : Declare the variables for base URI and request body
		String BaseURI="https://reqres.in/";
		String requestBody="{\r\n"
								+ "    \"name\": \"morpheus\",\r\n"
								+ "    \"job\": \"zion resident\"\r\n"
								+ "}";
		String Resource = "api/users/2";
						
//Step 2 : Declare BaseURI
		
RestAssured.baseURI=BaseURI;
						
//Step 3 Configure your request body and triger the api

String responsebody=
given().header("Content-Type","application/json").body(requestBody)
.when().put("api/users/2")
.then().extract().response().asString();
 System.out.println(responsebody);
						
given().header("Content-Type","application/json").body(requestBody).log().all().when().put("api/users/2")
.then().log().all().extract().response().asString();
						
//step 4 : create an object of JsonPath to parse the requestBody

JsonPath jsp_req= new JsonPath(requestBody);
String req_name=jsp_req.getString("name");
System.out.println("requestBody parameter name:"+req_name);
String req_job=jsp_req.getString("job");
System.out.println("requestBody parameter job:"+req_job);
						
//create an object of jsonpath to parse the responseBody
JsonPath jsp_res= new JsonPath(responsebody);
String res_name=jsp_res.getString("name");
System.out.println("responsebody parameter name:"+res_name);
String res_job=jsp_res.getString("job");
System.out.println("responsebody parameter job:"+res_job);
						
//step 5 : Validate the responsebody parameters
Assert.assertEquals(res_name, req_name);
Assert.assertEquals(res_job, res_job);
						
								
	}

}
