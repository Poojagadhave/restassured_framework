package RestassuredreferenceCopy;

import static io.restassured.RestAssured.given;

import org.testng.Assert;

import io.restassured.RestAssured;
import io.restassured.path.json.JsonPath;

public class Postreference {

	public static void main(String[] args) {
		
	//step 1 : Declare the variables for base uri and request body
		
	 String BaseURI="https://reqres.in/";

	 String requestBody="{\r\n"
						+ "    \"name\": \"morpheus\",\r\n"
						+ "    \"job\": \"leader\"\r\n"
						+ "}";

	//Step 2 : BaseURI

	RestAssured.baseURI=BaseURI;

	//Step 3 Configure your request body and triger the api
	/*String responsebody=given().header("Content-Type","application/json").body(requestBody).when().post("api/users")
	.then().extract().response().asString();
	System.out.println(responsebody);*/
				
	String responseBody=given().header("Content-Type","application/json").body(requestBody)
	.when().post("api/users")		
	.then().extract().response().asString();

	//create an object of jsonpath to parse the responseBody 
	JsonPath Jsp_res=new JsonPath(responseBody);
		 
	String res_name= Jsp_res.getString("name");
    System.out.println(res_name);

	 String res_job= Jsp_res.getString("job");
	 System.out.println(res_job);
		 
	String res_id= Jsp_res.getString("id");
	System.out.println(res_id);
		 
	//create an object of jsonPath to parse the requestbody
	JsonPath Jsp_req=new JsonPath(requestBody);
		 
	String req_name= Jsp_res.getString("name");
	System.out.println("requestbody parameter name:"+ res_name);

	String req_job= Jsp_res.getString("job");
	System.out.println("requestbody parameter job:"+ res_job);
		 
	String req_id= Jsp_res.getString("res_id");
	System.out.println("requestbody parameter id:"+ res_id);
		 
		 
	//Validate the responsebody parametrs;
	 Assert.assertEquals(res_name, "morpheus");
	 Assert.assertEquals(res_job, "leader");
	 Assert.assertNotNull(res_id);
		 

	}

}
