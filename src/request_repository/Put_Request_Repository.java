package request_repository;

import java.io.IOException;



import java.util.ArrayList;

import comman_utility_package.Excel_data_reader;

public class Put_Request_Repository extends Endpoints {
	public static String put_TC1_Request() throws IOException {
		
		ArrayList<String> excelData =Excel_data_reader.Read_Excel_Data("Api_Data.xlsx", "Put_API", "Put_TC_1");
		System.out.println(excelData);
		
		String req_name =excelData.get(1);
		
		String req_job =excelData.get(2);
		
		String requestBody = "{\r\n"
				+ "    \"name\": \""+req_name+"\",\r\n"
				+ "    \"job\": \""+req_job+"\"\r\n"
				+ "}";
		return requestBody;
	}

}
